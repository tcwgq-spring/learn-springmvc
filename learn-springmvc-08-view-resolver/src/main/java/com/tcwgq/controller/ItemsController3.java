package com.tcwgq.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tcwgq.po.Items;

/**
 * 基于注解开发的handler
 *
 * @author tcwgq
 * @time 2017年10月1日 上午10:07:01
 * @email tcwgq@outlook.com
 */
@Controller
public class ItemsController3 {

	@RequestMapping("/queryItems4")
	public ModelAndView queryItems() {

		List<Items> itemsList = new ArrayList<>();
		Items it1 = new Items();
		it1.setId(1);
		it1.setName("aaa");
		it1.setDetail("AAA");
		it1.setCreatetime(new Date());

		Items it2 = new Items();
		it2.setId(2);
		it2.setName("bbb");
		it2.setDetail("BBB");
		it2.setCreatetime(new Date());

		itemsList.add(it1);
		itemsList.add(it2);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/itemsList");
		return modelAndView;
	}
}
