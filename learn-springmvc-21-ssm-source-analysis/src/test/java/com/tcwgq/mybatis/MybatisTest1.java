package com.tcwgq.mybatis;

import com.tcwgq.mapper.UserMapper;
import com.tcwgq.model.User;
import com.tcwgq.po.UserVo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * mapper代理形式
 *
 * @author tcwgq
 * @time 2017年9月10日上午9:58:45
 * @email tcwgq@outlook.com
 */
public class MybatisTest1 {
    private SqlSessionFactory sqlSessionFactory;

    private UserMapper mapper;

    @Before
    public void setUp() throws Exception {
        // 不能加classpath前缀
        InputStream is = Resources.getResourceAsStream("mybatis-config-test.xml");
        // Configuration config = new Configuration();
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = sqlSessionFactory.openSession();

        mapper = session.getMapper(UserMapper.class);
    }

    @Test
    public void testInsertUser() {
        fail("Not yet implemented");
    }

    @Test
    public void testGetById() throws Exception {
        User user = mapper.getById(10);
        System.out.println(user);
    }

    @Test
    public void testGetByName() {
        fail("Not yet implemented");
    }

    @Test
    public void find() throws Exception {
        UserVo vo = new UserVo();
        vo.setUsername("张");
        vo.setSex("2");
        vo.setOffset(0);
        vo.setSize(10);
        vo.setSort("id");
        List<User> users = mapper.find(vo);
        System.out.println(users);
    }

    @Test
    public void testUpdateUser() {
        fail("Not yet implemented");
    }

    @Test
    public void testDeleteById() {
        fail("Not yet implemented");
    }
}