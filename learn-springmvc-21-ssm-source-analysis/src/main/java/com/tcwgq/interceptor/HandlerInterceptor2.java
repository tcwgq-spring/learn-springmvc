package com.tcwgq.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author tcwgq
 * @time 2017年10月23日 下午8:06:00
 * @email tcwgq@outlook.com
 */
public class HandlerInterceptor2 implements HandlerInterceptor {

	// 执行handler方法之前
	// 身份认证，身份授权
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		System.out.println("HandlerInterceptor2...preHandle");

		// return true表示放行
		return true;
	}

	// 执行handler方法之后，返回ModelAndView之前
	// 公用的数据可以放在这里
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("HandlerInterceptor2...postHandle");
	}

	// 执行handler方法之后
	// 统一的异常处理，统一的日志处理
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		System.out.println("HandlerInterceptor2...afterCompletion");
	}

}
