package com.tcwgq.circledepend;

/**
 * @author tcwgq
 * @time 2020/9/3 11:43
 */
public class Parent {
    private Child child;

    public Parent() {
    }

    public Parent(Child child) {
        this.child = child;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }
}
