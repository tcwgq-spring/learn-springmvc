package com.tcwgq.controller;

import com.tcwgq.controller.validator.ValidationGroup1;
import com.tcwgq.exception.CustomException;
import com.tcwgq.po.ItemsCustom;
import com.tcwgq.po.ItemsQueryVo;
import com.tcwgq.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Controller参数绑定
 * 
 * @author tcwgq
 * @time 2017年10月1日 上午10:07:01
 * @email tcwgq@outlook.com
 */
@Controller
// 指定公共前缀
@RequestMapping("/items")
public class ItemsController {

	@Autowired
	private ItemsService service;

	// 复合类型的参数绑定
	@RequestMapping("/queryItems")
	public ModelAndView queryItems(ItemsQueryVo itemsQueryVo) throws Exception {
		List<ItemsCustom> itemsList = service.findItemsList(itemsQueryVo);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/itemsList");
		return modelAndView;
	}

	// 简单类型的参数绑定
	@RequestMapping(value = "/editItems", method = { RequestMethod.GET, RequestMethod.POST })
	public String editItems(Model model, @RequestParam(value = "id", required = true) Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		if (itemsCustom == null) {
			throw new CustomException("商品不存在");
		}

		model.addAttribute("items", itemsCustom);

		return "/items/editItems";
	}

	// 绑定pojo
	@RequestMapping("/editItemsSubmit")
	// 要求页面的name属性与pojo属性名称一致
	// 需要校验的pojo前面添加@Validated
	// BindingResult result用来接收校验出错的信息
	// @Validated和BindingResult是配对出现的
	// 只用ValidationGroup1的校验规则
	/**
	 * springmvc默认支持pojo数据回显，springmvc自动将形参中的pojo重新放回request域中，request的key为pojo的类名（首字母小写）
	 * 
	 * @ModelAttribute("items")用来指定存储在request域中的key
	 * 
	 * 默认不支持简单类型的数据回显，简单类型需要使用model进行回显
	 * 
	 * @param model
	 * @param id
	 * @param itemsCustom
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public String editItemsSubmit(ModelMap model, Integer id, @Validated(value = { ValidationGroup1.class }) @ModelAttribute("items") ItemsCustom itemsCustom,
			BindingResult result, MultipartFile items_pic) throws Exception {
		// 获取校验错误信息
		if (result.hasErrors()) {
			List<ObjectError> allErrors = result.getAllErrors();
			for (ObjectError error : allErrors) {
				System.out.println(error.getDefaultMessage());
			}
			model.addAttribute("allErrors", allErrors);
			// 使用model回显数据是最简单的方法
			// model.addAttribute("items", itemsCustom);
			return "/items/editItems";
		}

		if (items_pic != null) {
			String path = "D:/Temp/";
			String originalFilename = items_pic.getOriginalFilename();
			String newFilename = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));
			File newFile = new File(path + newFilename);
			items_pic.transferTo(newFile);
			itemsCustom.setPic(newFilename);
		}
		service.updateItems(id, itemsCustom);
		return "success";
	}

	// 商品分类
	// itemtypes表示最终将方法返回值放在request中的key
	@ModelAttribute("itemtypes")
	public Map<String, String> getItemTypes() {

		Map<String, String> itemTypes = new HashMap<String, String>();
		itemTypes.put("101", "数码");
		itemTypes.put("102", "母婴");

		return itemTypes;
	}

	// 绑定数组
	@RequestMapping("/deleteItems")
	public String deleteItems(Integer[] ids) throws Exception {
		// 删除逻辑
		return "success";
	}

	@RequestMapping("/editItemsQuery")
	public ModelAndView editItemsQuery(ItemsQueryVo itemsQueryVo) throws Exception {
		List<ItemsCustom> itemsList = service.findItemsList(itemsQueryVo);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/editItemsQuery");
		return modelAndView;
	}

	// 绑定 List类型
	// 通过ItemsQueryVo接收批量的商品信息，存储到itemsList中
	@RequestMapping("/editItemsQuerySubmit")
	public String editItemsQuerySubmit(ItemsQueryVo itemsQueryVo) throws Exception {
		return "success";
	}

	@RequestMapping("/editItemsMap")
	public ModelAndView editItemsMap(@RequestParam Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsCustom", itemsCustom);
		modelAndView.setViewName("/items/editItemsMap");
		return modelAndView;
	}

	// 绑定Map类型
	@RequestMapping("/editItemsMapSubmit")
	public String editItemsMapSubmit(ItemsQueryVo itemsQueryVo) throws Exception {

		return "success";
	}

	// restful
	// url模板的url
	@RequestMapping("/get/{id}")
	public @ResponseBody ItemsCustom restful(@PathVariable("id") Integer id) throws Exception {
		ItemsCustom custom = service.findItemsById(id);

		return custom;
	}

}
