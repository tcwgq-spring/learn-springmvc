package com.tcwgq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * @author tcwgq
 * @time 2017年10月23日 下午9:03:25
 * @email tcwgq@outlook.com
 */
@Controller
public class LoginController {
	@RequestMapping("/login")
	public String login(HttpSession session, String username, String password) {

		session.setAttribute("username", username);

		return "redirect:/items/queryItems.action";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {

		session.invalidate();

		return "redirect:/items/queryItems.action";
	}
}
