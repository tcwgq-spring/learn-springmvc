package com.tcwgq.beanaware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:25:22
 * @email tcwgq@outlook.com
 */
public class Person implements BeanNameAware, BeanFactoryAware, InitializingBean, DisposableBean {
    private String name;

    public Person() {
        System.out.println("Person 无参构造方法...");
    }

    public Person(String name) {
        System.out.println("Person 有参构造方法...");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("Person setName...");
        this.name = name;
    }

    public void add() {
        System.out.println("Person add...");
    }

    public void personInit() {
        System.out.println("Person init...");
    }

    public void personDestroy() {
        System.out.println("Person destroy...");
    }

    @Override
    public String toString() {
        return "Person [name=" + name + "]";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean afterPropertiesSet...");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("BeanFactoryAware setBeanFactory...");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("BeanNameAware setBeanName...");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean destroy...");
    }

}
