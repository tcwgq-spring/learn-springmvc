package com.tcwgq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tcwgq.po.ItemsCustom;

/**
 * @author tcwgq
 * @time 2017年10月22日 下午9:11:07
 * @email tcwgq@outlook.com
 */
@Controller
public class RequestJson {
	// 请求商品信息，返回商品信息
	@RequestMapping("/requestJson")
	public @ResponseBody ItemsCustom reqestJson(@RequestBody ItemsCustom custom) {
		return custom;
	}

	// 请求key/value，输出json
	@RequestMapping("/responseJson")
	public @ResponseBody ItemsCustom responseJson(ItemsCustom custom) {
		return custom;
	}
}
