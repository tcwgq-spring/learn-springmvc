package com.tcwgq.syncer;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.mapper.PersonPlusMapper;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;
import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月26日 上午8:05:46
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class PersonCommonTask {
	private static final Integer BATCH_SIZE = 10;

	@Autowired
	private PersonMapper personMapper;

	@Autowired
	private PersonPlusMapper personPlusMapper;

	public void sync() {
		int totalCount = personMapper.findCount(null);
		int batch_times = totalCount % BATCH_SIZE == 0 ? totalCount / BATCH_SIZE : totalCount / BATCH_SIZE + 1;
		int pageNo = 1;
		while (pageNo <= batch_times) {
			int offset = (pageNo - 1) * BATCH_SIZE;
			int pageSize = BATCH_SIZE;
			PersonCondition condition = new PersonCondition();
			condition.setOffset(offset);
			condition.setPageSize(pageSize);
			List<Person> persons = personMapper.find(condition);
			for (Person person : persons) {
				PersonPlus personPlus = new PersonPlus();
				personPlus.setName(person.getName());
				personPlusMapper.insert(personPlus);
			}
			pageNo++;
		}
	}

	@Test
	public void test1() {
		sync();
	}
}
