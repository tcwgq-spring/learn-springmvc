package com.tcwgq.syncer;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import com.tcwgq.sync.task.PersonSyncCallableTask;
import com.tcwgq.sync.task.PersonSyncRunnableTask;
import com.tcwgq.sync.task.ThreadPoolSyncer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.mapper.PersonPlusMapper;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;

/**
 * @author tcwgq
 * @time 2017年10月26日 下午8:30:59
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class SyncTest {
	private static final int BATCH_SIZE = 10;
	@Autowired
	private PersonMapper personMapper;
	@Autowired
	private PersonPlusMapper personPlusMapper;

	@Test
	public void test1() throws InterruptedException {
		int index = 0;

		while (true) {
			System.out.println("第" + (index + 1) + "批次");

			int offset = index * BATCH_SIZE;
			int pageSize = BATCH_SIZE;

			System.out.println(offset + "-" + pageSize);

			Executors.newSingleThreadExecutor().execute(new PersonSyncRunnableTask(offset, pageSize, personMapper, personPlusMapper));

//			new Thread(new PersonSyncRunnableTask(offset, pageSize, personMapper, personPlusMapper)).start();

			Thread.sleep(1000l);

			PersonCondition condition = new PersonCondition();
			condition.setOffset(offset);
			condition.setPageSize(pageSize);
			List<Person> persons = personMapper.find(condition);

			if (persons.size() <= 0) {
				break;
			}

			index++;
		}
	}

	@Test
	public void test2() throws InterruptedException {
		int index = 0;
		BlockingQueue<Callable<String>> queue = new ArrayBlockingQueue<>(100);

		while (true) {
			System.out.println("第" + (index + 1) + "批次");
			int offset = index * BATCH_SIZE;
			int pageSize = BATCH_SIZE;
			System.out.println("offset-pageSize:" + offset + "-" + pageSize);
			PersonCondition condition = new PersonCondition();
			condition.setOffset(offset);
			condition.setPageSize(pageSize);
			List<Person> persons = personMapper.find(condition);
			System.out.println("size: " + persons.size());
			if (persons.size() <= 0) {
				break;
			}

			queue.add(new PersonSyncCallableTask(offset, pageSize, personMapper, personPlusMapper));

			index++;
		}

		ThreadPoolSyncer.getInstance().invokeAll(queue);
	}
}
