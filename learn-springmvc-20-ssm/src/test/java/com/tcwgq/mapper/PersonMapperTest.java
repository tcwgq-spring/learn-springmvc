package com.tcwgq.mapper;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSONObject;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:50:03
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class PersonMapperTest {
	@Autowired
	private PersonMapper mapper;

	@Test
	public void testInsert() {
		for (int i = 1; i <= 1000; i++) {
			Person person = new Person();
			person.setName("jack" + i);
			person.setAge(i);
			mapper.insert(person);
		}
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		Person person = mapper.get(10);
		System.out.println(person);
	}

	@Test
	public void testFindCount() {
		PersonCondition condition = new PersonCondition();
		condition = null;
		int count = mapper.findCount(condition);
		System.out.println(count);
	}

	@Test
	public void testFind() {
		PersonCondition condition = new PersonCondition();
		condition.setOffset(0);
		condition.setPageSize(5);
		List<Person> persons = mapper.find(condition);
		System.out.println(persons.size());
		System.out.println(JSONObject.toJSONString(persons));
	}

}
