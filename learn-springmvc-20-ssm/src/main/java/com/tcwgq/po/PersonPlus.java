package com.tcwgq.po;

import java.io.Serializable;
import java.util.Date;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:45:32
 * @email tcwgq@outlook.com
 */
public class PersonPlus implements Serializable {
	private Integer id;
	private String name;
	private Date timeCreated;
	private Date timeModified;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	@Override
	public String toString() {
		return "PersonPlus [id=" + id + ", name=" + name + ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + "]";
	}

}
