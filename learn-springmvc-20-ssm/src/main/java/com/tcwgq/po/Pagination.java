package com.tcwgq.po;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tcwgq
 * @time 2017年11月1日 下午9:52:33
 * @email tcwgq@outlook.com
 */
public class Pagination<T> {
	private int draw;
	private int recordsTotal;
	private int recordsFiltered;
	private int totalCount;
	private List<T> dataList = new ArrayList<>();

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "Pagination [draw=" + draw + ", recordsTotal=" + recordsTotal + ", recordsFiltered=" + recordsFiltered + ", totalCount=" + totalCount
				+ ", dataList=" + dataList + "]";
	}

}
