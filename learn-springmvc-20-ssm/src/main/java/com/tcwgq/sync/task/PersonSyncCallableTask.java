package com.tcwgq.sync.task;

import java.util.List;
import java.util.concurrent.Callable;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.mapper.PersonPlusMapper;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;
import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月26日 上午8:20:05
 * @email tcwgq@outlook.com
 */
public class PersonSyncCallableTask implements Callable<String> {

	private Integer offset;
	private Integer pageSize;
	private PersonMapper personMapper;
	private PersonPlusMapper personPlusMapper;

	public PersonSyncCallableTask(Integer offset, Integer pageSize, PersonMapper personMapper, PersonPlusMapper personPlusMapper) {
		this.offset = offset;
		this.pageSize = pageSize;
		this.personMapper = personMapper;
		this.personPlusMapper = personPlusMapper;
	}

	@Override
	public String call() throws Exception {
		PersonCondition condition = new PersonCondition();
		condition.setOffset(offset);
		condition.setPageSize(pageSize);
		List<Person> persons = personMapper.find(condition);
		for (Person person : persons) {
			System.out.println("id: " + person.getId());
			PersonPlus personPlus = new PersonPlus();
			personPlus.setName(person.getName());
			personPlusMapper.insert(personPlus);
		}
		return "success";
	}

}
