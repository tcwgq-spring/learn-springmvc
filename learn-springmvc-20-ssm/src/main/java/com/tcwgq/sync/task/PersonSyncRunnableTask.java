package com.tcwgq.sync.task;

import java.util.List;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.mapper.PersonPlusMapper;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;
import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月26日 上午8:19:35
 * @email tcwgq@outlook.com
 */
public class PersonSyncRunnableTask implements Runnable {

	private final Integer offset;
	private final Integer pageSize;
	private final PersonMapper personMapper;
	private final PersonPlusMapper personPlusMapper;

	public PersonSyncRunnableTask(final Integer offset, final Integer pageSize, final PersonMapper personMapper, final PersonPlusMapper personPlusMapper) {
		this.offset = offset;
		this.pageSize = pageSize;
		this.personMapper = personMapper;
		this.personPlusMapper = personPlusMapper;
	}

	@Override
	public void run() {
		PersonCondition condition = new PersonCondition();
		condition.setOffset(offset);
		condition.setPageSize(pageSize);
		List<Person> persons = personMapper.find(condition);
		for (Person person : persons) {
			PersonPlus personPlus = new PersonPlus();
			personPlus.setName(person.getName());
			personPlusMapper.insert(personPlus);
		}
	}

}
