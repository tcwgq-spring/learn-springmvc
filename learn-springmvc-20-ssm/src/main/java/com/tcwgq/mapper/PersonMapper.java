package com.tcwgq.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:08:39
 * @email tcwgq@outlook.com
 */
@Repository
public interface PersonMapper {

	public int insert(Person person);

	public int delete(Integer id);

	public Person get(Integer id);

	public int findCount(PersonCondition condition);

	public List<Person> find(PersonCondition condition);

}
