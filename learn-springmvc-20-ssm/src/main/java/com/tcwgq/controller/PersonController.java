package com.tcwgq.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.po.Pagination;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;

/**
 * @author tcwgq
 * @time 2017年11月1日 下午9:36:08
 * @email tcwgq@outlook.com
 */
@Controller
@RequestMapping("/person")
public class PersonController {
	@Autowired
	private PersonMapper personMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list() {
		return "person/list";
	}
	
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public String persons() {
		return "person/persons";
	}

	@ResponseBody
	@RequestMapping(value = "/getByCriteria", method = RequestMethod.GET)
	public Pagination<Person> getByCriteria(
			@RequestParam(value = "draw", required = false) Integer draw,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "length", required = false) Integer length,
			@RequestParam(value = "id", required = false) Integer id, 
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "age", required = false) Integer age, 
			@RequestParam(value = "timeCreatedStart", required = false) Date timeCreatedStart,
			@RequestParam(value = "timeCreatedEnd", required = false) Date timeCreatedEnd,
			@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {

		PersonCondition condition = new PersonCondition();
		condition.setId(id);
		condition.setName(name);
		condition.setAge(age);
		condition.setTimeCreatedStart(timeCreatedStart);
		condition.setTimeCreatedEnd(timeCreatedEnd);

		pageNo = pageNo <= 0 ? 1 : pageNo;
		pageSize = pageSize <= 0 ? 10 : pageSize;
		int offset = (pageNo - 1) * pageSize;

		condition.setOffset(start);
		condition.setPageSize(length);
		int totalCount = personMapper.findCount(condition);
		List<Person> persons = personMapper.find(condition);
		Pagination<Person> pagination = new Pagination<>();
		pagination.setDraw(draw);
		pagination.setRecordsTotal(totalCount);
		pagination.setRecordsFiltered(totalCount);
		pagination.setTotalCount(totalCount);
		pagination.setDataList(persons);
		return pagination;
	}
}
