package com.tcwgq.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author tcwgq
 * @time 2017年10月17日 下午9:42:15
 * @email tcwgq@outlook.com
 */
public class CustomExceptionResolver implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {

		CustomException exception = null;
		if (ex instanceof CustomException) {
			exception = (CustomException) ex;
		} else {
			exception = new CustomException("未知错误");
		}

		String message = exception.getMessage();

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("message", message);
		modelAndView.setViewName("error");
		return modelAndView;
	}

}
