package com.tcwgq.service;

import com.tcwgq.dao.UserDao;

/**
 * @author tcwgq
 * @time 2017年8月15日上午8:25:45
 * @email tcwgq@outlook.com
 */
public class UserService {
	private String action;
	private UserDao userDao;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void add() {
		userDao.add();
	}
}
