package com.tcwgq.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.model.Student;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:27:13
 * @email tcwgq@outlook.com
 */
public class IOCTest1 {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		Student student = (Student) ctx.getBean("student");
		System.out.println(student);
	}
}
