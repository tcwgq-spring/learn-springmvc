package com.tcwgq.factory;

import com.tcwgq.model.Student;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:24:01
 * @email tcwgq@outlook.com
 */
public class StudentFactory {
	public static Student getStudent() {
		return new Student();
	}
}
