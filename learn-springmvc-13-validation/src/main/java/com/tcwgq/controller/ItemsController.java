package com.tcwgq.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tcwgq.controller.validator.ValidationGroup1;
import com.tcwgq.po.ItemsCustom;
import com.tcwgq.po.ItemsQueryVo;
import com.tcwgq.service.ItemsService;

/**
 * Controller参数绑定
 * 
 * @author tcwgq
 * @time 2017年10月1日 上午10:07:01
 * @email tcwgq@outlook.com
 */
@Controller
// 指定公共前缀
@RequestMapping("/items")
public class ItemsController {

	@Autowired
	private ItemsService service;

	// 复合类型的参数绑定
	@RequestMapping("/queryItems")
	public ModelAndView queryItems(ItemsQueryVo itemsQueryVo) throws Exception {
		List<ItemsCustom> itemsList = service.findItemsList(itemsQueryVo);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/itemsList");
		return modelAndView;
	}

	// 简单类型的参数绑定
	@RequestMapping(value = "/editItems", method = { RequestMethod.GET, RequestMethod.POST })
	public String editItems(Model model, @RequestParam(value = "id", required = true) Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		model.addAttribute("itemsCustom", itemsCustom);

		return "/items/editItems";
	}

	// 绑定pojo
	@RequestMapping("/editItemsSubmit")
	// 要求页面的name属性与pojo属性名称一致
	// 需要校验的pojo前面添加@Validated
	// BindingResult result用来接收校验出错的信息
	// @Validated和BindingResult是配对出现的
	// 只用ValidationGroup1的校验规则
	public String editItemsSubmit(ModelMap model, Integer id, @Validated(value = { ValidationGroup1.class }) ItemsCustom itemsCustom, BindingResult result)
			throws Exception {
		// 获取校验错误信息
		if (result.hasErrors()) {
			List<ObjectError> allErrors = result.getAllErrors();
			for (ObjectError error : allErrors) {
				System.out.println(error.getDefaultMessage());
			}
			model.addAttribute("allErrors", allErrors);
			return "/items/editItems";
		}

		service.updateItems(id, itemsCustom);
		return "success";
	}

	// 绑定数组
	@RequestMapping("/deleteItems")
	public String deleteItems(Integer[] ids) throws Exception {
		// 删除逻辑
		return "success";
	}

	@RequestMapping("/editItemsQuery")
	public ModelAndView editItemsQuery(ItemsQueryVo itemsQueryVo) throws Exception {
		List<ItemsCustom> itemsList = service.findItemsList(itemsQueryVo);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/editItemsQuery");
		return modelAndView;
	}

	// 绑定 List类型
	// 通过ItemsQueryVo接收批量的商品信息，存储到itemsList中
	@RequestMapping("/editItemsQuerySubmit")
	public String editItemsQuerySubmit(ItemsQueryVo itemsQueryVo) throws Exception {
		return "success";
	}

	@RequestMapping("/editItemsMap")
	public ModelAndView editItemsMap(@RequestParam Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsCustom", itemsCustom);
		modelAndView.setViewName("/items/editItemsMap");
		return modelAndView;
	}

	// 绑定Map类型
	@RequestMapping("/editItemsMapSubmit")
	public String editItemsMapSubmit(ItemsQueryVo itemsQueryVo) throws Exception {

		return "success";
	}

}
