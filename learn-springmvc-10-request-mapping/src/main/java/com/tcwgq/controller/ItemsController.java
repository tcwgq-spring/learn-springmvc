package com.tcwgq.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tcwgq.po.ItemsCustom;
import com.tcwgq.service.ItemsService;

/**
 * 基于注解开发的handler
 *
 * @author tcwgq
 * @time 2017年10月1日 上午10:07:01
 * @email tcwgq@outlook.com
 */
@Controller
// 指定公共前缀
@RequestMapping("/items")
public class ItemsController {

	@Autowired
	private ItemsService service;

	@RequestMapping("/queryItems")
	public ModelAndView queryItems() throws Exception {

		List<ItemsCustom> itemsList = service.findItemsList(null);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/itemsList");
		return modelAndView;
	}

	// 指定访问路径与请求方式
	@RequestMapping(value = "/editItems", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView editItems(Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsCustom", itemsCustom);
		modelAndView.setViewName("/items/editItems");
		return modelAndView;
	}

	@RequestMapping("/editItemsSubmit")
	public ModelAndView editItemsSubmit(Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsCustom", itemsCustom);
		modelAndView.setViewName("/items/editItems");
		return modelAndView;
	}
}
