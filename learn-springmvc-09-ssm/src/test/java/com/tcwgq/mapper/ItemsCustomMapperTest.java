package com.tcwgq.mapper;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tcwgq.po.ItemsCustom;

/**
 * @author tcwgq
 * @time 2017年10月6日 下午3:16:45
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ItemsCustomMapperTest {

	@Autowired
	ItemsCustomMapper mapper;

	@Test
	public void testFindItemsList() throws Exception {
		List<ItemsCustom> list = mapper.findItemsList(null);
		System.out.println(list);
	}

}
