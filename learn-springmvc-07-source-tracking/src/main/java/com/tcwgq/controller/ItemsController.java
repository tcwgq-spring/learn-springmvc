package com.tcwgq.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.tcwgq.po.Items;

/**
 * 基于SimpleControllerHandlerAdapter的handler
 * 
 * @author tcwgq
 * @time 2017年10月1日 上午10:12:54
 * @email tcwgq@outlook.com
 */
public class ItemsController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		List<Items> itemsList = new ArrayList<>();
		Items it1 = new Items();
		it1.setId(1);
		it1.setName("aaa");
		it1.setDetail("AAA");
		it1.setCreatetime(new Date());

		Items it2 = new Items();
		it2.setId(2);
		it2.setName("bbb");
		it2.setDetail("BBB");
		it2.setCreatetime(new Date());

		itemsList.add(it1);
		itemsList.add(it2);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/WEB-INF/jsp/items/itemsList.jsp");
		return modelAndView;
	}

}
