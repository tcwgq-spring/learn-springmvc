package com.tcwgq.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.tcwgq.po.Items;

/**
 * 基于HttpRequestHandlerAdapter的handler
 * 
 * @author tcwgq
 * @time 2017年10月1日 上午10:10:27
 * @email tcwgq@outlook.com
 */
public class ItemsController2 implements HttpRequestHandler {

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Items> itemsList = new ArrayList<>();
		Items it1 = new Items();
		it1.setId(1);
		it1.setName("aaa");
		it1.setDetail("AAA");
		it1.setCreatetime(new Date());

		Items it2 = new Items();
		it2.setId(2);
		it2.setName("bbb");
		it2.setDetail("BBB");
		it2.setCreatetime(new Date());

		itemsList.add(it1);
		itemsList.add(it2);

		request.setAttribute("itemsList", itemsList);
		request.getRequestDispatcher("/WEB-INF/jsp/items/itemsList.jsp").forward(request, response);
		/**
		 * 此种方式可以实现原始的servlet功能 使用此方法可以通过修改response，设置响应的数据格式，比如响应json数据
		 */
	}

}
